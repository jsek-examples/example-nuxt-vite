# Vuetify + Nuxt.js + Vite

### Limitations

- requires Yarn
- cannot use `variables.sass` for Vuetify
- `treeShake` for Vuetify needs to be conditional for production build

### Run locally

```bash
# install dependencies
$ yarn install

# serve with hot reload at localhost:4000
$ yarn dev
```

Expected result:

![](./screenshot.jpg)
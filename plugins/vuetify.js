import colors from 'vuetify/es5/util/colors';

export default {
  breakpoint: {
    thresholds: {
      xs: 600,
      sm: 960,
      md: 1280,
      lg: 1800
    },
  },
  theme: {
    options: {
      customProperties: true
    },
    dark: false,
    themes: {
      light: {
        primary: colors.purple.darken1,
        secondary: colors.blueGrey.darken1,
        accent: '#FF6142',
        error: colors.red.accent3,
        info: colors.lightBlue.darken1,
        warning: colors.amber.darken2,
        success: colors.green.base,
        grey: colors.grey.base,
        orange: colors.orange.base,
        white: colors.shades.white,
        darkGrey: colors.grey.darken4,
        background: '#EBEBEB',
      },
    },
  },
};
export default {
  ssr: false,
  target: 'static',
  head: {
    titleTemplate: '%s - my-nuxt-app',
    title: 'my-nuxt-app',
    htmlAttrs: { lang: 'en' },
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [{ rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }],
    link: [{ rel: 'preconnect', href: 'https://fonts.googleapis.co' }],
    link: [{ rel: 'preconnect', href: 'https://fonts.gstatic.com' }],
    link: [{ rel: 'stylesheet', href: 'https://fonts.googleapis.com/css2?family=Sora:wght@400;500;700&display=swap' }],
  },
  css: [
    '@/assets/main.sass',
  ],
  plugins: [
  ],
  components: [{ path: '~/components', pathPrefix: false }],
  buildModules: [
    '@nuxt/typescript-build',
    '@nuxtjs/vuetify',
    'nuxt-vite',
  ],
  modules: [
    '@nuxtjs/axios',
  ],
  vuetify: {
    treeShake: process.env.NODE_ENV === 'production',
    optionsPath: './plugins/vuetify.js',
  },
}
